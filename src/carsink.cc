#include <carsink.h>
#include "carsource.h"
#include "car_m.h"

Define_Module(CarSink);

void CarSink::initialize() {
    m_position = static_cast<Position>(static_cast<int>(par("position")));
}

void CarSink::handleMessage(cMessage* msg) {
    Car* car = dynamic_cast<Car*>(msg);
    if (!car) {
        //we didn't get a car object....just delete it and continue;
        delete msg;
        return;
    }

    std::string sourcePosition= printPosition(car->getSource());
    ev << "\nRecieved car at " << printPosition(m_position) << " from " << sourcePosition;

    delete car;
}
