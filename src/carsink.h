#ifndef CARSINK_H_
#define CARSINK_H_

#include <omnetpp.h>
#include "util.h"

class CarSink : public cSimpleModule
{
protected:
    virtual void initialize();
    virtual void handleMessage(cMessage* msg);

private:
    Position m_position;
};

#endif
