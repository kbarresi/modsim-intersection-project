#include "carsource.h"
#include "car_m.h"

Define_Module(CarSource);

void CarSource::initialize()
{
    m_position = static_cast<Position>(static_cast<int>(par("position")));
    ev << "Car source initializing... " << printPosition(m_position);

    scheduleAt(simTime(), static_cast<cMessage*>(new Car));
}

void CarSource::handleMessage(cMessage* msg) {
    Car* car = static_cast<Car*>(msg);
    car->setSource(m_position);
    car->setDestination(static_cast<int>(randomPosition()));
    ev << "\tGenerated car\tDestination: " << printPosition(car->getDestination()) << "\n";

    send(car, "sourceOut");

    double carDelay = par("carDelay");
    scheduleAt(simTime() + carDelay, static_cast<cMessage*>(new Car));
}

Position CarSource::randomPosition() {
    while (1) { //keep going until you eventually get a good one
        Position selection = static_cast<Position>(rand() % 4);
        if (m_position == selection)
            continue;
        else
            return selection;
    }
}
