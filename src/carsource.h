#ifndef __INTERSECTION_CARSOURCESINK_H_
#define __INTERSECTION_CARSOURCESINK_H_

#include <omnetpp.h>
#include "util.h"

class CarSource : public cSimpleModule
{
protected:
    virtual void initialize();
    virtual void handleMessage(cMessage* msg);

private:
    Position randomPosition();
    Position m_position;
};


#endif
