#include <lightcontroller.h>
#include "controlmessage_m.h"

Define_Module(LightController);

void LightController::initialize() {
    m_lightOneState = Green;
    m_lightTwoState = Red;

    m_lightDelayTime = par("lightDelayTime");
    ev << "Traffic light delay time: " << m_lightDelayTime;

    ControlMessage *lightOneInit = new ControlMessage();
    lightOneInit->setControlCode(ToGreen);

    ControlMessage *lightTwoInit = new ControlMessage();
    lightTwoInit->setControlCode(ToRed);

    send(lightOneInit, "lightOne");
    send(lightTwoInit, "lightTwo");

    scheduleAt(simTime() + m_lightDelayTime, new cMessage());
}

void LightController::handleMessage(cMessage* msg) {
    switchLights();

    delete msg;
    scheduleAt(simTime() + m_lightDelayTime, new cMessage());
}

void LightController::switchLights() {
    ev << "***CHANGING LIGHTS!";
    ControlMessage *lightOneChange = new ControlMessage();
    lightOneChange->setControlCode(ToggleLight);

    ControlMessage *lightTwoChange = new ControlMessage();
    lightTwoChange->setControlCode(ToggleLight);

    send(lightOneChange, "lightOne");
    send(lightTwoChange, "lightTwo");
}
