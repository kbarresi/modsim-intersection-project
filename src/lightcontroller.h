#ifndef LIGHTCONTROLLER_H_
#define LIGHTCONTROLLER_H_

#include <omnetpp.h>
#include "lightpair.h"

class LightController : public cSimpleModule
{
  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);

  private:
    int m_lightDelayTime;
    LightStatus m_lightOneState;
    LightStatus m_lightTwoState;

    void switchLights();
};

#endif
