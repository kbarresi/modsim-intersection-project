#include <lightpair.h>
#include "car_m.h"
#include "controlmessage_m.h"

Define_Module(LightPair);

int LightPair::NUMBER_OF_QUEUES = 2;
void LightPair::initialize()
{
    m_isVertical = par("orientation") == "vertical";

    m_queues = (cQueue**) malloc(sizeof(cQueue*) * NUMBER_OF_QUEUES);

    for (int i = 0; i < NUMBER_OF_QUEUES; i++)
        m_queues[i] = new cQueue();
}
void LightPair::finish() {
    int queueOneSize = m_queues[0]->length();
    int queueTwoSize = m_queues[1]->length();

    for (int i = 0; i < NUMBER_OF_QUEUES; i++) {
        while (!m_queues[i]->empty())   //clear messages
            delete m_queues[i]->pop();
        delete m_queues[i];
    }

    free(m_queues);


    ev << "\nCars in queue one: " << queueOneSize;
    ev << "\nCars in queue two: " << queueTwoSize;
}
void LightPair::handleMessage(cMessage *msg)
{
    ev << "Cars in queue: " << (m_queues[0]->length() + m_queues[1]->length());
    int id = 0;

    if (Car* car = dynamic_cast<Car*>(msg)) {  //We got a car
        switch (m_lightStatus) {
        case Green:
            id = destinationToGateIndex(static_cast<Position>(car->getDestination()));
            send(car, "carOut", id);
            ev << "\nRecieved car going to: " << printPosition(static_cast<Position>(car->getDestination())) << "\n";
            break;
        case Red:   //red light: add to queue;
            id = destinationToGateIndex(static_cast<Position>(car->getArrivalGateId()));
            if (0 <= id && id <= NUMBER_OF_QUEUES)
                m_queues[id]->insert(car);
            break;
        default:
            break;
        }
        return;
    }


    if (ControlMessage* ctrl = dynamic_cast<ControlMessage*>(msg)) {    //it's a control message
        ControlType type = static_cast<ControlType>(ctrl->getControlCode());
        if (type == ToggleLight)
            toggleLight();
        else if (type == ToRed)
            changeLight(Red);
        else if (type == ToGreen)
            changeLight(Green);
        delete ctrl;
        return;
    }
}

void LightPair::toggleLight() {
    if (m_lightStatus == Green)
        changeLight(Red);
    else if (m_lightStatus == Red)
        changeLight(Green);
}
void LightPair::changeLight(LightStatus status) {
    m_lightStatus = status;

    if (m_lightStatus == Green) {
        while (drainCarQueue()) {

        }
    }
}

bool LightPair::drainCarQueue() {
    if (m_lightStatus != Green)
        return false;

    //send out another car from each queue, if available;
    Car* carOne = 0;
    Car* carTwo = 0;

    if (!m_queues[0]->empty())
        carOne = static_cast<Car*>(m_queues[0]->pop());
    if (!m_queues[1]->empty())
        carTwo = static_cast<Car*>(m_queues[1]->pop());



    if (carOne) {
        int index = destinationToGateIndex(static_cast<Position>(carOne->getDestination()));
        send(carOne, "carOut", index);
    }
    if (carTwo) {
        int index = destinationToGateIndex(static_cast<Position>(carTwo->getDestination()));
        send(carTwo, "carOut", index);
    }

    if (!carOne && !carTwo)
        return false;
    return true;
}

int LightPair::destinationToGateIndex(Position dst) {
    if (m_isVertical) {
        if (dst == North)
            return 0;
        else    //Otherwise it's south
            return 1;
    } else {    //horizontal...
        if (dst == East)
            return 0;
        else
            return 1;
    }
}
