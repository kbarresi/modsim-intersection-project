#ifndef __INTERSECTION_LIGHTPAIR_H_
#define __INTERSECTION_LIGHTPAIR_H_

#include "util.h"
#include <omnetpp.h>

enum LightStatus { Red, Green };
enum ControlType { ToggleLight, ToRed, ToGreen };

class LightPair : public cSimpleModule
{
  protected:
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage *msg);

    void toggleLight();
    void changeLight(LightStatus status);

    bool drainCarQueue();

  private:
    LightStatus m_lightStatus;
    cQueue** m_queues;

    bool m_isVertical;
    static int NUMBER_OF_QUEUES;


    int destinationToGateIndex(Position dst);
};

#endif
