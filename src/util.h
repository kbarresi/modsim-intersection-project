#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <string>

enum Position { North = 0,
                East = 1,
                South = 2,
                West = 3 };


inline std::string printPosition(Position p) {
    if (p == North)
        return "North";
    else if (p == East)
        return "East";
    else if (p == South)
        return "South";
    else if (p == West)
        return "West";
    else
        return " [UNKNOWN_POSITION]";
}

inline std::string printPosition(int p) {
    return printPosition(static_cast<Position>(p));
}

#endif
